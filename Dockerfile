FROM registry.gitlab.com/redlinekg/redline-node-container
WORKDIR /app/
COPY ./package.json ./yarn.lock /app/
RUN yarn install && yarn cache clean
ENV NODE_OPTIONS=--max_old_space_size=8192
COPY . /app
RUN yarn build
EXPOSE 8080
CMD yarn start:prod
