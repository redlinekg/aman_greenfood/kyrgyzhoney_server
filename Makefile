TAG=registry.gitlab.com/redlinekg/aman_greenfood/kyrgyzhoney_server:latest
CADDY_TAG=registry.gitlab.com/redlinekg/aman_greenfood/kyrgyzhoney_server/caddy:latest
NGINX_TAG=registry.gitlab.com/redlinekg/aman_greenfood/kyrgyzhoney_server/nginx-imaginary-cache:latest

.PHONY: build_server build_caddy publish_server publish_caddy deploy build all

all: build deploy

build_server:
	telegram-send '[kyrgyzhoney build_server] start'
	docker build -t $(TAG) .
	telegram-send '[kyrgyzhoney build_server] end'

publish_server:
	telegram-send '[kyrgyzhoney publish_server] start'
	docker push $(TAG)
	telegram-send '[kyrgyzhoney publish_server] end'

build_caddy:
	telegram-send '[kyrgyzhoney build_caddy] start'
	docker build -f caddy.Dockerfile -t $(CADDY_TAG) .
	telegram-send '[kyrgyzhoney build_caddy] end'

publish_caddy:
	telegram-send '[kyrgyzhoney publish_caddy] start'
	docker push $(CADDY_TAG)
	telegram-send '[kyrgyzhoney publish_caddy] end'


build_nginx:
	telegram-send '[orionhotel build_nginx_cache] start'
	docker build -f nginx.Dockerfile -t $(NGINX_TAG) .
	telegram-send '[orionhotel build_nginx_cache] end'

publish_nginx:
	telegram-send '[orionhotel publish_nginx] start'
	docker push $(NGINX_TAG)
	telegram-send '[orionhotel publish_nginx] end'


deploy:
	telegram-send '[kyrgyzhoney deploy] start'
	docker stack deploy --with-registry-auth --compose-file ./docker-compose.yml --compose-file ./docker-compose.prod.yml kyrgyzhoney
	telegram-send '[kyrgyzhoney deploy] end'

build: build_server build_caddy publish_server publish_caddy
