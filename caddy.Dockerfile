FROM caddy:builder AS builder

RUN caddy-builder \
    github.com/caddyserver/json5-adapter

FROM caddy:alpine
COPY --from=builder /usr/bin/caddy /usr/bin/caddy

COPY ./caddy.json5 /etc/caddy/Caddyfile
ENTRYPOINT ["caddy"]
ENV DISABLE_HTTPS=false DISABLE_HTTPS_REDIRECTS=false
CMD ["run", "--config", "/etc/caddy/Caddyfile", "--adapter", "json5"]