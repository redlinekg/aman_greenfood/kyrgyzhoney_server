FROM nginx:alpine
RUN /bin/sh -c 'mkdir /var/nginx; mkdir /var/nginx/cache'
COPY nginx.conf /etc/nginx/conf.d/default.conf
