import { Test, TestingModule } from '@nestjs/testing';
import { FilesaverService } from './filesaver.service';

describe('FilesaverService', () => {
    let service: FilesaverService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [FilesaverService],
        }).compile();

        service = module.get<FilesaverService>(FilesaverService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
