import * as crypto from "crypto";

import { Injectable, NotFoundException } from "@nestjs/common";
import { exists, readFile, stat, writeFile } from "fs";
import { extname, resolve as fs_resolve, join } from "path";

import { ConfigService } from "../config/config.service";
import { promisify } from "util";
import { resolve as url_resolve } from "url";

const _asyncWriteFile = promisify(writeFile);
const _asyncReadFile = promisify(readFile);
const _asyncExist = promisify(exists);
const _asyncStat = promisify(stat);

@Injectable()
export class FilesaverService {
  public constructor(private readonly config_service: ConfigService) {}

  /**
   * Save file to uploads folder
   */
  public asyncWriteFile = (filePath: string, buffer: Buffer): Promise<void> => {
    return _asyncWriteFile(this.getRelativePath(filePath), buffer);
  };

  /**
   * Read file from uploads folder
   */
  public asyncReadFile = (filePath: string): Promise<Buffer> => {
    return _asyncReadFile(this.getRelativePath(filePath));
  };

  public getHash(data: Buffer): string {
    const hasher = crypto.createHash("sha1");
    hasher.update(data);
    return hasher.digest("hex");
  }

  public async getSize(filePath: string): Promise<number> {
    try {
      const stat = await _asyncStat(await this.getAbsolutePath(filePath));
      return stat.size;
    } catch (error) {
      return 0;
    }
  }

  public async getExtension(filePath: string): Promise<string> {
    return extname(filePath);
  }

  /**
   * Get relative path
   * @param filePath
   */
  public getRelativePath(filePath: string): string {
    return join("uploads", filePath);
  }

  /**
   *
   * @param filePath
   */
  public getUrlPath(filePath: string): string {
    return "/" + join("uploads", filePath);
  }

  /**
   * Get path to file on system
   * @param filePath
   */
  public async getAbsolutePath(filePath: string): Promise<string> {
    try {
      const path = fs_resolve("uploads", filePath);
      const exist = await _asyncExist(path);
      if (!exist) {
        throw new NotFoundException("File is not exist");
      }
      return path;
    } catch (error) {
      return filePath;
    }
  }

  /**
   * Get path to file on system
   * @param filePath
   */
  public getUrlAbsolutePath(filePath: string): string {
    const url_path = this.getUrlPath(filePath);
    return url_resolve(this.config_service.get("MEDIA_ORIGIN"), url_path);
  }
}
