import { Module, Global } from '@nestjs/common';
import { FilesaverService } from './filesaver/filesaver.service';
import { ConfigService } from './config/config.service';

@Global()
@Module({
    providers: [
        FilesaverService,
        {
            provide: ConfigService,
            useValue: new ConfigService(),
        },
    ],
    exports: [FilesaverService, ConfigService],
})
export class UtilsModule {}
