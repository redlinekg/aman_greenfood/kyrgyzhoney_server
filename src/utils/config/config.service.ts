import * as dotenv from "dotenv";

import { Injectable } from "@nestjs/common";

@Injectable()
export class ConfigService {
  public constructor() {
    let c = dotenv.config();
  }

  public get(key: string): string {
    return process.env[key];
  }
}
