import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@vlzh/nest-typeorm-i18n";
import { I18nRepository } from "typeorm-i18n";
import { CustomTypeOrmService } from "../CustomTypeOrmService";
import { Slide } from "./slide.entity";

@Injectable()
export class SlidesService extends CustomTypeOrmService<Slide> {
  constructor(@InjectRepository(Slide) readonly repo: I18nRepository<Slide>) {
    super(repo);
  }
}
