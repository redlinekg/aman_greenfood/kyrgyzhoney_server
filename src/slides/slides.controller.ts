import { Controller } from "@nestjs/common";
import { Crud, CrudRequest, Override, ParsedRequest } from "@nestjsx/crud";
import { Slide } from "./slide.entity";
import { SlidesService } from "./slides.service";

@Crud({
  model: {
    type: Slide
  },
  query: {
    join: {
      meta_image: {
        eager: true
      },
      image: {
        eager: true
      },
      video: {
        eager: true
      },
      image_mobile: {
        eager: true
      }
    }
  },
  routes: {
    updateOneBase: {
      allowParamsOverride: true
    }
  },
  params: {
    id: {
      field: "id",
      type: "string",
      primary: true
    }
  }
})
@Controller("api/slides")
export class SlidesController {
  constructor(private readonly service: SlidesService) {}
}
