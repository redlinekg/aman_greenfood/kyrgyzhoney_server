import { DEFAULT_LANG, LANGUAGES_LIST } from "../constances";
import { Column, Entity, ManyToOne } from "typeorm";
import { I18nColumn } from "typeorm-i18n";
import { BaseSeoEntity } from "../base.entity";
import { UploadsItem } from "../uploads/uploads.entity";

@Entity()
export class Slide extends BaseSeoEntity {
  // title
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  title: string;
  // title2
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  title2: string;
  // sub_title
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  sub_title: string;
  // link
  @Column({
    nullable: true
  })
  link: string;
  // button_text
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column({
    nullable: true
  })
  button_text: string;
  // description
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  description: string;
  // image
  @ManyToOne(() => UploadsItem)
  image: UploadsItem;
  // background_image
  @ManyToOne(() => UploadsItem)
  background_image: UploadsItem;
  // video
  @ManyToOne(() => UploadsItem, {
    nullable: true
  })
  video: UploadsItem;
  // background_color
  @Column({
    nullable: true
  })
  background_color: string;
}
