import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@vlzh/nest-typeorm-i18n";
import { Slide } from "./slide.entity";
import { SlidesController } from "./slides.controller";
import { SlidesService } from "./slides.service";

@Module({
  imports: [TypeOrmModule.forFeature([Slide])],
  controllers: [SlidesController],
  providers: [SlidesService]
})
export class SlidesModule {}
