import {
    Body,
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    Post,
    Res,
    Req,
    UseGuards,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { LoginDto } from './auth.dto';
import { AuthService } from './auth.service';

@Controller('api/auth')
export class AuthController {
    public constructor(
        private readonly authService: AuthService,
        private readonly jwtService: JwtService,
    ) {}

    @HttpCode(HttpStatus.OK)
    @Post('login')
    public async login(
        @Body() body: LoginDto,
        @Res() res: Response,
    ): Promise<void> {
        const user = await this.authService.validateUserByCredentials(body);
        const token = this.jwtService.sign({
            email: user.email,
        });
        res.cookie('token', token);
        res.end(token);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('me')
    public me(@Req() req) {
        return req.user;
    }
}
