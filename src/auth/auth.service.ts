import { BadRequestException, Injectable } from '@nestjs/common';
import { compare } from 'bcrypt';
import { User } from '../users/users.entity';
import { UsersService } from '../users/users.service';
import { LoginDto } from './auth.dto';

@Injectable()
export class AuthService {
    public constructor(private readonly usersService: UsersService) {}
    public async validateUserByCredentials(data: LoginDto): Promise<User> {
        const user = await this.usersService.findOneByEmail(data.email);
        if (!user)
            throw new BadRequestException(
                `User with email: ${data.email} not found`,
            );
        const eq = await compare(data.password, user.password);
        if (!eq) throw new BadRequestException('Password is incorrect');
        return user;
    }
}
