import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-jwt';
import { UsersService } from '../users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    public constructor(private readonly usersService: UsersService) {
        super({
            secretOrKey: 'secret',
            jwtFromRequest: req => {
                return req.cookies && req.cookies.token
                    ? req.cookies.token
                    : req.headers['authentication'];
            },
        });
    }

    public async validate({ email }: any): Promise<any> {
        const user = await this.usersService.findOneByEmail(email);
        if (!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}
