import { CookieParserMiddleware } from "@nest-middlewares/cookie-parser";
import { NestFactory } from "@nestjs/core";
import { CrudConfigService } from "@nestjsx/crud";
import { ConfigService } from "./utils/config/config.service";
import { LocaleCrudInterceptor } from "./interceptors";

CrudConfigService.load({
  routes: {
    getManyBase: {
      interceptors: [LocaleCrudInterceptor],
      decorators: [],
    },
    getOneBase: {
      interceptors: [LocaleCrudInterceptor],
      decorators: [],
    },
  },
});

import { AppModule } from "./app.module";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(new CookieParserMiddleware().use);
  const cs = app.get(ConfigService);
  await app.listen(cs.get("PORT"), cs.get("HOST"));
}
bootstrap();
