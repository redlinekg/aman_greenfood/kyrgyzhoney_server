import { Controller } from "@nestjs/common";
import { Crud, CrudRequest, Override, ParsedRequest } from "@nestjsx/crud";
import { Page } from "./page.entity";
import { PagesService } from "./pages.service";

@Crud({
  model: {
    type: Page
  },
  query: {
    join: {
      meta_image: {
        eager: true
      }
    }
  },
  routes: {
    updateOneBase: {
      allowParamsOverride: true
    }
  },
  params: {
    id: {
      field: "id",
      type: "string",
      primary: true
    }
  }
})
@Controller("api/pages")
export class PagesController {
  constructor(private readonly service: PagesService) {}

  @Override("getOneBase")
  public getByIdOrSlug(@ParsedRequest() req: CrudRequest): Promise<Page> {
    if (!parseInt((req.parsed.search.$and[1] as any).id.$eq)) {
      req.parsed.search.$and.push({
        slug: (req.parsed.search.$and[1] as any).id.$eq
      });
      delete req.parsed.search.$and[1];
    }
    return this.service.getOne(req);
  }
}
