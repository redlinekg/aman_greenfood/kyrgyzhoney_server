import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@vlzh/nest-typeorm-i18n";
import { Page } from "./page.entity";
import { PagesController } from "./pages.controller";
import { PagesService } from "./pages.service";

@Module({
  imports: [TypeOrmModule.forFeature([Page])],
  controllers: [PagesController],
  providers: [PagesService]
})
export class PagesModule {}
