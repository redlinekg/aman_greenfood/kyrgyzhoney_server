import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@vlzh/nest-typeorm-i18n";
import { I18nRepository } from "typeorm-i18n";
import { CustomTypeOrmService } from "../CustomTypeOrmService";
import { Page } from "./page.entity";

@Injectable()
export class PagesService extends CustomTypeOrmService<Page> {
  constructor(@InjectRepository(Page) readonly repo: I18nRepository<Page>) {
    super(repo);
  }
}
