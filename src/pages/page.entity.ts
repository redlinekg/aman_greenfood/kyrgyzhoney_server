import { Column, Entity, ManyToOne } from "typeorm";
import { I18nColumn } from "typeorm-i18n";
import { BaseSeoEntity } from "../base.entity";
import { DEFAULT_LANG, LANGUAGES_LIST } from "../constances";
import { UploadsItem } from "../uploads/uploads.entity";

@Entity()
export class Page extends BaseSeoEntity {
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  title: string;
  @Column({
    unique: true
  })
  slug: string;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  alt_title: string;
  @Column({
    default: false
  })
  is_about_company: boolean;
  @ManyToOne(() => UploadsItem, {
    nullable: true
  })
  header_background: UploadsItem;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  body: string;
}
