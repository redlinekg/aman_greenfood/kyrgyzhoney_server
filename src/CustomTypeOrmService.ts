import { CrudRequestOptions } from "@nestjsx/crud";
import { ParsedRequestParams } from "@nestjsx/crud-request";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { I18nRepository } from "typeorm-i18n";

export class CustomTypeOrmService<T> extends TypeOrmCrudService<T> {
  constructor(protected repo: I18nRepository<T>) {
    super(repo);
  }
  createBuilder(
    parsed: ParsedRequestParams,
    options: CrudRequestOptions,
    many = true
  ) {
    this.repo.locale((options as any).language);
    return super.createBuilder(parsed, options, many);
  }
}
