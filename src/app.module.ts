import { MailerModule } from "@nest-modules/mailer";
import { Module } from "@nestjs/common";
import { TelegramModule } from "nest-telegram-bot";
import { TypeOrmModule } from "@vlzh/nest-typeorm-i18n";
import { LoggerModule } from "nestjs-pino-logger";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import { BlogModule } from "./blog/blog.module";
import { ContactsModule } from "./contacts/contacts.module";
import { CatalogModule } from "./catalog/catalog.module";
import { NotificationsModule } from "./notifications/notifications.module";
import { PagesModule } from "./pages/pages.module";
import { ReviewsModule } from "./reviews/reviews.module";
import { SettingsModule } from "./settings/settings.module";
import { SlidesModule } from "./slides/slides.module";
import { UploadsModule } from "./uploads/uploads.module";
import { ConfigService } from "./utils/config/config.service";
import { UtilsModule } from "./utils/utils.module";
import { UsersModule } from "./users/users.module";
import { AuthModule } from "./auth/auth.module";

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [UtilsModule],
      useFactory: async (
        configService: ConfigService
      ): Promise<PostgresConnectionOptions> => ({
        type: "postgres",
        host: configService.get("DB_HOST"),
        database: configService.get("DB_NAME"),
        username: configService.get("DB_USER"),
        password: configService.get("DB_PASSWORD"),
        entities: [__dirname + "/**/*.entity{.ts,.js}"],
        synchronize: true,
      }),
      inject: [ConfigService],
    }),
    TelegramModule.forRootAsync({
      imports: [UtilsModule],
      inject: [ConfigService],
      useFactory(configService: ConfigService) {
        return {
          token: configService.get("TELEGRAM_BOT_TOKEN"),
          chat_id: configService.get("TELEGRAM_CHAT_ID"),
        };
      },
    }),
    MailerModule.forRootAsync({
      imports: [UtilsModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const email = configService.get("EMAIL_SMTP_EMAIL");
        const pass = configService.get("EMAIL_SMTP_PASSWORD");
        const host = configService.get("EMAIL_SMTP_HOST");
        const sender = configService.get("EMAIL_SENDER");
        return {
          transport: `smtps://${email}:${pass}@${host}`,
          defaults: {
            from: `${sender} <${email}>`,
          },
        };
      },
    }),
    LoggerModule.forRoot({
      prettyPrint: true,
      level: "debug",
    }),
    BlogModule,
    UtilsModule,
    UploadsModule,
    ContactsModule,
    CatalogModule,
    PagesModule,
    ReviewsModule,
    SlidesModule,
    SettingsModule,
    NotificationsModule,
    UsersModule,
    AuthModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
