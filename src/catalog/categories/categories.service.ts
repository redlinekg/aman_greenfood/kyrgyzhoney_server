import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@vlzh/nest-typeorm-i18n";
import { CustomTypeOrmService } from "../../CustomTypeOrmService";
import { I18nRepository } from "typeorm-i18n";
import { Category } from "./category.entity";

@Injectable()
export class CategoriesService extends CustomTypeOrmService<Category> {
  constructor(
    @InjectRepository(Category) readonly repo: I18nRepository<Category>
  ) {
    super(repo);
  }
}
