import { Column, Entity, ManyToOne, OneToMany } from "typeorm";

import { BaseSeoEntity } from "../../base.entity";
import { I18nColumn } from "typeorm-i18n";
import { DEFAULT_LANG, LANGUAGES_LIST } from "../../constances";
import { Product } from "../products/product.entity";

@Entity("categories")
export class Category extends BaseSeoEntity {
  // title
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  title: string;

  // title
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column({
    unique: true
  })
  slug: string;

  // alt title
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  alt_title: string;

  // description
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  description: string;

  // products
  @OneToMany(
    () => Product,
    product => product.category
  )
  products: Product[];
}
