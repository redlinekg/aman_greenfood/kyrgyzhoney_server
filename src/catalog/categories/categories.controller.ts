import { Controller } from "@nestjs/common";
import { Crud } from "@nestjsx/crud";
import { Category } from "./category.entity";
import { CategoriesService } from "./categories.service";

@Crud({
  model: {
    type: Category,
  },
  query: {
    join: {
      avatar: {
        eager: true,
      },
      products: {
        eager: false,
      },
      "products.images": {
        eager: false,
        alias: "images",
      },
      "products.image": {
        eager: false,
      },
    },
  },
})
@Controller("api/categories")
export class CategoriesController {
  constructor(readonly service: CategoriesService) {}
}
