import { DEFAULT_LANG, LANGUAGES_LIST } from "../../constances";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { I18nColumn } from "typeorm-i18n";
import { Product } from "./product.entity";

@Entity()
export class ProductPropertyEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  type: string;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  value: string;
  @ManyToOne(() => Product, {
    onDelete: "CASCADE"
  })
  product: Product;
}
