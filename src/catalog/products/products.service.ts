import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@vlzh/nest-typeorm-i18n";
import { I18nRepository } from "typeorm-i18n";
import { CustomTypeOrmService } from "../../CustomTypeOrmService";
import { Product } from "./product.entity";

@Injectable()
export class ProductsService extends CustomTypeOrmService<Product> {
  constructor(
    @InjectRepository(Product) readonly repo: I18nRepository<Product>
  ) {
    super(repo);
  }
}
