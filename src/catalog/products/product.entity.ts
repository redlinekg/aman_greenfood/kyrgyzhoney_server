import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { I18nColumn } from "typeorm-i18n";
import { BaseSeoEntity } from "../../base.entity";
import { DEFAULT_LANG, LANGUAGES_LIST } from "../../constances";
import { UploadsItem } from "../../uploads/uploads.entity";
import { Category } from "../categories/category.entity";
import { ProductPropertyEntity } from "./product_property.entity";

@Entity("products")
export class Product extends BaseSeoEntity {
  // title
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST,
  })
  @Column()
  title: string;

  // slug
  @Column({ unique: true })
  slug: string;

  // category
  @ManyToOne(() => Category)
  category: Category;

  // description
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST,
  })
  @Column({ type: "text" })
  description: string;

  // volume
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST,
  })
  @Column()
  volume: string;

  // main image
  @ManyToOne(() => UploadsItem, {
    nullable: true,
  })
  image: UploadsItem;

  // header background
  @ManyToOne(() => UploadsItem)
  header_background: UploadsItem;

  // Закрепить на главной
  @Column({
    default: false,
  })
  pin: boolean;

  // body
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST,
  })
  @Column({ type: "text", nullable: true })
  body: string;

  // properties
  @OneToMany(
    () => ProductPropertyEntity,
    (prop) => prop.product,
    {
      cascade: true,
    }
  )
  public properties: ProductPropertyEntity[];

  // video
  @ManyToOne(() => UploadsItem, {
    nullable: true,
  })
  public video: UploadsItem;

  // gallery
  @ManyToMany(() => UploadsItem)
  @JoinTable()
  public images: UploadsItem[];
}
