import { Controller } from "@nestjs/common";
import { Crud, CrudRequest, Override, ParsedRequest } from "@nestjsx/crud";
import { Product } from "./product.entity";
import { ProductsService } from "./products.service";

@Crud({
  model: {
    type: Product
  },
  query: {
    join: {
      category: {
        eager: true
      },
      header_background: {
        eager: true
      },
      meta_image: {
        eager: true
      },
      image: {
        eager: true
      },
      video: {
        eager: true
      },
      properties: {
        eager: true
      },
      images: {
        eager: true
      }
    },
    sort: [
      {
        field: "images.id",
        order: "ASC"
      }
    ]
  },
  routes: {
    updateOneBase: {
      allowParamsOverride: true
    }
  },
  params: {
    id: {
      field: "id",
      type: "string",
      primary: true
    }
  }
})
@Controller("api/products")
export class ProductsController {
  constructor(readonly service: ProductsService) {}

  @Override("getOneBase")
  public getByIdOrSlug(@ParsedRequest() req: CrudRequest): Promise<Product> {
    if (!parseInt((req.parsed.search.$and[1] as any).id.$eq)) {
      req.parsed.search.$and.push({
        slug: (req.parsed.search.$and[1] as any).id.$eq
      });
      delete req.parsed.search.$and[1];
    }
    return this.service.getOne(req);
  }
}
