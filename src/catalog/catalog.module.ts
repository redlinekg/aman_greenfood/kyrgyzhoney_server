import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@vlzh/nest-typeorm-i18n";

import { Product } from "./products/product.entity";
import { ProductsController } from "./products/products.controller";
import { ProductsService } from "./products/products.service";

import { CategoriesController } from "./categories/categories.controller";
import { CategoriesService } from "./categories/categories.service";
import { Category } from "./categories/category.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Product, Category])],
  controllers: [ProductsController, CategoriesController],
  providers: [ProductsService, CategoriesService],
  exports: [ProductsService]
})
export class CatalogModule {}
