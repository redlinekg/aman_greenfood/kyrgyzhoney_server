import {
  Column,
  CreateDateColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import { I18nColumn } from "typeorm-i18n";
import { UploadsItem } from "./uploads/uploads.entity";
import { LANGUAGES_LIST, DEFAULT_LANG } from "./constances";

export class BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;
  @CreateDateColumn()
  public created_at: Date;
  @UpdateDateColumn()
  public updated_at: Date;
  public type?: string;
}

export class BaseSeoEntity extends BaseEntity {
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column({
    default: ""
  })
  public meta_title: string;
  @ManyToOne(() => UploadsItem, {
    eager: true,
    nullable: true
  })
  public meta_image: UploadsItem;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column({
    default: ""
  })
  public meta_description: string;
}
