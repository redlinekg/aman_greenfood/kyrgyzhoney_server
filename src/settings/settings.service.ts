import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@vlzh/nest-typeorm-i18n";
import { LogManager } from "nestjs-pino-logger/dist";
import { Logger } from "pino";
import { Repository } from "typeorm";
import { SettingsProperty } from "./property.entity";
import { SectionDto } from "./section.dto";
import { SettingsSection } from "./section.entity";

@Injectable()
export class SettingsService {
  private log: Logger;

  public constructor(
    @InjectRepository(SettingsSection)
    private readonly repo: Repository<SettingsSection>,
    @InjectRepository(SettingsProperty)
    private readonly prop_repo: Repository<SettingsProperty>,
    readonly logManager: LogManager
  ) {
    this.log = logManager.getLogger(SettingsService);
  }

  public async getSections(): Promise<SettingsSection[]> {
    const sections = await this.repo.find();
    return sections;
  }

  public async getProperies(): Promise<SettingsProperty[]> {
    const sections = await this.getSections();
    return sections.reduce((acc, section) => {
      return [...acc, ...section.properties];
    }, []);
  }

  private async deleteProperiesWithoutSection() {
    const properties_without_section = await this.prop_repo.find({
      where: {
        section: null
      }
    });
    try {
      await this.prop_repo.delete(properties_without_section.map(p => p.id));
    } catch (error) {
      this.log.error(
        `error on attempt to delete all properties without section\nError: ${error.message}`
      );
    }
  }

  public async saveSections(
    sections: SectionDto[]
  ): Promise<SettingsSection[]> {
    const _sections: SettingsSection[] = [];
    await this.deleteProperiesWithoutSection();
    for (const section of sections) {
      _sections.push(this.repo.create(section));
    }
    await this.repo.save(_sections);
    await this.repo
      .createQueryBuilder()
      .delete()
      .where("id NOT IN (:...ids)", {
        ids: _sections.map(s => s.id)
      })
      .execute();
    return _sections;
  }
}
