import { Column, PrimaryGeneratedColumn, Entity, ManyToOne } from 'typeorm';
import { SettingsSection } from './section.entity';

@Entity()
export class SettingsProperty {
    @PrimaryGeneratedColumn()
    public id: number;
    @Column({
        nullable: true,
    })
    public position?: number;
    @Column({
        unique: true,
    })
    public name: string;
    @Column()
    public description: string;
    @Column()
    public type: string;
    @Column({
        type: 'text',
    })
    public value: string;
    //
    @ManyToOne(() => SettingsSection, section => section.properties, {
        onDelete: 'CASCADE',
    })
    public section: SettingsSection;
}
