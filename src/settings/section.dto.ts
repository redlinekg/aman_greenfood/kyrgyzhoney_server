import { PropertyDto } from './property.dto';

export class SectionDto {
    public id?: number;
    public position?: number;
    public title: string;
    public description: string;
    public properties: PropertyDto[];
}
