import { Body, Controller, Get, Post } from '@nestjs/common';
import { SectionDto } from './section.dto';
import { SettingsSection } from './section.entity';
import { SettingsService } from './settings.service';
import { SettingsProperty } from './property.entity';

@Controller('api/settings')
export class SettingsController {
    public constructor(private readonly settingsService: SettingsService) {}
    @Post()
    public postSettings(@Body() body: SectionDto[]) {
        return this.settingsService.saveSections(body);
    }
    @Get()
    public getSections(): Promise<SettingsSection[]> {
        return this.settingsService.getSections();
    }
    @Get('properties')
    public getProperties(): Promise<SettingsProperty[]> {
        return this.settingsService.getProperies();
    }
}
