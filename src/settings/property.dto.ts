export class PropertyDto {
    public id?: number;
    public position?: number;
    public name: string;
    public description: string;
    public type: string;
    public value: string;
}
