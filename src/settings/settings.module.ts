import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@vlzh/nest-typeorm-i18n";
import { SettingsProperty } from "./property.entity";
import { SettingsSection } from "./section.entity";
import { SettingsController } from "./settings.controller";
import { SettingsService } from "./settings.service";

@Module({
  imports: [TypeOrmModule.forFeature([SettingsSection, SettingsProperty])],
  controllers: [SettingsController],
  providers: [SettingsService]
})
export class SettingsModule {}
