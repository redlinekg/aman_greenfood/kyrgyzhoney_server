import { PrimaryGeneratedColumn, Column, Entity, OneToMany } from 'typeorm';
import { SettingsProperty } from './property.entity';

@Entity()
export class SettingsSection {
    @PrimaryGeneratedColumn()
    public id: number;
    @Column({
        nullable: true,
    })
    public position?: number;
    @Column()
    public title: string;
    @Column({
        type: 'text',
        default: '',
    })
    public description: string;
    // properties
    @OneToMany(() => SettingsProperty, property => property.section, {
        cascade: true,
        eager: true,
    })
    public properties: SettingsProperty[];
}
