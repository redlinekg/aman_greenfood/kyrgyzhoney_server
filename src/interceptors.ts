import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor
} from "@nestjs/common";
import { Request } from "express";

export const LANGUAGE_REQUEST_KEY = "language";
export const PARSED_CRUD_REQUEST_KEY = "NESTJSX_PARSED_CRUD_REQUEST_KEY";

@Injectable()
export class LocaleCrudInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler) {
    const req = context.switchToHttp().getRequest() as Request;
    if (req[PARSED_CRUD_REQUEST_KEY]) {
      req[PARSED_CRUD_REQUEST_KEY].options[LANGUAGE_REQUEST_KEY] =
        req.cookies["next-i18next"];
    }
    return next.handle();
  }
}
