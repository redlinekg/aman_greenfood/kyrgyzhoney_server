import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@vlzh/nest-typeorm-i18n";
import { UploadsController } from "./uploads.controller";
import { UploadsItem } from "./uploads.entity";
import { UploadsService } from "./uploads.service";
import { UploadsSubscriber } from "./uploads.subscriber";

@Module({
  imports: [TypeOrmModule.forFeature([UploadsItem])],
  providers: [UploadsService, UploadsSubscriber],
  controllers: [UploadsController],
  exports: [UploadsService]
})
export class UploadsModule {}
