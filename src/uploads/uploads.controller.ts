import {
    Controller,
    Get,
    Param,
    Post,
    Res,
    UploadedFile,
    UseInterceptors,
    HttpCode,
} from '@nestjs/common';
import { basename } from 'path';
import { FileInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';
import { FilesaverService } from '../utils/filesaver/filesaver.service';
import { UploadsService } from './uploads.service';
import { FileEditorjsDto } from './file-editorjs.dto';

interface MulterFile {
    fieldname: string;
    originalname: string;
    encoding: string;
    mimetype: string;
    buffer: Buffer;
    size: number;
}

@Controller('api/uploads')
export class UploadsController {
    public constructor(
        private readonly uploadsService: UploadsService,
        private readonly filesaverService: FilesaverService,
    ) {}
    @Post('file')
    @UseInterceptors(FileInterceptor('file'))
    public uploadFile(@UploadedFile() file: MulterFile) {
        return this.uploadsService.saveFile(file.buffer, file.mimetype, false);
    }
    @Post('file_editorjs')
    @HttpCode(200)
    @UseInterceptors(FileInterceptor('file'))
    public async uploadFileEditorJs(
        @UploadedFile() file: MulterFile,
    ): Promise<FileEditorjsDto> {
        const saved_file = await this.uploadsService.saveFile(
            file.buffer,
            file.mimetype,
            false,
        );
        return {
            success: 1,
            file: {
                extension: saved_file.ext.slice(1),
                name: basename(saved_file.path),
                url: saved_file.absolute_path,
                size: saved_file.size,
            },
        };
    }
    @Post('image')
    @UseInterceptors(FileInterceptor('file'))
    public uploadImage(@UploadedFile() file: MulterFile) {
        return this.uploadsService.saveFile(file.buffer, file.mimetype, true);
    }
    @Get(':filePath')
    public async getFile(
        @Param('filePath') filePath: string,
        @Res() res: Response,
    ): Promise<void> {
        res.sendFile(await this.filesaverService.getAbsolutePath(filePath));
    }
}
