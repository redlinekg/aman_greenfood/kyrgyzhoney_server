import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class UploadsItem {
    @PrimaryGeneratedColumn()
    public id: number;
    @Column()
    public isImage: boolean;
    @Column()
    public path: string;
    // computed fields:
    public url_path?: string;
    public absolute_path?: string;
    public ext?: string;
    public size?: number;
}
