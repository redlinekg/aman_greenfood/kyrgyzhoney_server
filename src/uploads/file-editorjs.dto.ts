interface FileEditorjs {
    url: string;
    name: string;
    extension: string;
    size: number;
}
export class FileEditorjsDto {
    public success: number;
    public file: FileEditorjs;
}
