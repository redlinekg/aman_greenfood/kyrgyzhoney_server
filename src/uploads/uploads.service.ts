import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@vlzh/nest-typeorm-i18n";
import { getExtension } from "mime";
import { join } from "path";
import { Repository } from "typeorm";
import { FilesaverService } from "../utils/filesaver/filesaver.service";
import { UploadsItem } from "./uploads.entity";

@Injectable()
export class UploadsService {
  public constructor(
    private readonly filesaverService: FilesaverService,
    @InjectRepository(UploadsItem)
    private readonly uploadsItemRepository: Repository<UploadsItem>
  ) {}

  public async saveFile(
    buffer: Buffer,
    mimetype: string,
    isImage: boolean,
    folderPath: string = ""
  ): Promise<UploadsItem> {
    const hash = this.filesaverService.getHash(buffer);
    const ext = getExtension(mimetype);
    const filename = `${hash}.${ext}`;
    const filepath = join(folderPath, filename);
    this.filesaverService.asyncWriteFile(filepath, buffer);
    const uploadsItem = this.uploadsItemRepository.create({
      isImage: isImage,
      path: filepath
    });
    await this.uploadsItemRepository.save(uploadsItem);
    return await this.uploadsItemRepository.findOne(uploadsItem.id);
  }

  public async getPath(item: UploadsItem | number): Promise<string> {
    if (typeof item === "number") {
      item = await this.getItemById(item);
    }
    return this.filesaverService.getRelativePath(item.path);
  }

  public async getBufferForItem(item: UploadsItem | number): Promise<Buffer> {
    let file_path = "";
    if (item instanceof UploadsItem) {
      file_path = item.path;
    } else {
      item = await this.getItemById(item);
      file_path = item.path;
    }
    return this.filesaverService.asyncReadFile(file_path);
  }

  private async getItemById(id: number): Promise<UploadsItem> {
    const item = await this.uploadsItemRepository.findOne(id);
    return item;
  }
}
