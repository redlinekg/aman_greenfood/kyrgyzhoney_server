import { Injectable } from "@nestjs/common";
import { InjectConnection } from "@vlzh/nest-typeorm-i18n";
import { LogManager } from "nestjs-pino-logger/dist";
import { Logger } from "pino";
import { Connection, EntitySubscriberInterface } from "typeorm";
import { FilesaverService } from "../utils/filesaver/filesaver.service";
import { UploadsItem } from "./uploads.entity";

@Injectable()
export class UploadsSubscriber
  implements EntitySubscriberInterface<UploadsItem> {
  private log: Logger;
  public constructor(
    @InjectConnection() readonly connection: Connection,
    private readonly filesaver_service: FilesaverService,
    readonly logManager: LogManager
  ) {
    this.log = logManager.getLogger(UploadsSubscriber);
    connection.subscribers.push(this);
  }

  public listenTo() {
    return UploadsItem;
  }

  public async afterLoad(entity: UploadsItem): Promise<void> {
    this.log.trace(
      `Get paths for 'UploadsItem'; UploadsItem.path: ${entity.path}`
    );
    if (entity.path) {
      entity.url_path = this.filesaver_service.getUrlPath(entity.path);
      entity.absolute_path = this.filesaver_service.getUrlAbsolutePath(
        entity.path
      );
      entity.size = await this.filesaver_service.getSize(entity.path);
      entity.ext = await this.filesaver_service.getExtension(entity.path);
    }
  }
}
