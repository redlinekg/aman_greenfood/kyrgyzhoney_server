import { Column, Entity, ManyToOne } from "typeorm";
import { I18nColumn } from "typeorm-i18n";
import { BaseSeoEntity } from "../../base.entity";
import { DEFAULT_LANG, LANGUAGES_LIST } from "../../constances";
import { UploadsItem } from "../../uploads/uploads.entity";

@Entity("authors")
export class Author extends BaseSeoEntity {
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  title: string;
  @ManyToOne(() => UploadsItem)
  avatar: UploadsItem;
}
