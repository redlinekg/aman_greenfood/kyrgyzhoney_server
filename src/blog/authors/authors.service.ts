import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@vlzh/nest-typeorm-i18n";
import { I18nRepository } from "typeorm-i18n";
import { CustomTypeOrmService } from "../../CustomTypeOrmService";
import { Author } from "./author.entity";

@Injectable()
export class AuthorsService extends CustomTypeOrmService<Author> {
  constructor(@InjectRepository(Author) readonly repo: I18nRepository<Author>) {
    super(repo);
  }
}
