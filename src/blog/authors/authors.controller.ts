import { Controller } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { Author } from './author.entity';
import { AuthorsService } from './authors.service';

@Crud({
    model: {
        type: Author,
    },
    query: {
        join: {
            avatar: {
                eager: true,
            },
        },
    },
})
@Controller('api/article-authors')
export class AuthorsController {
    constructor(readonly service: AuthorsService) {}
}
