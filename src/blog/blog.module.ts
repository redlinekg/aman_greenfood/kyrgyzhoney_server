import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@vlzh/nest-typeorm-i18n";
import { Article } from "./articles/article.entity";
import { ArticlesController } from "./articles/articles.controller";
import { ArticlesService } from "./articles/articles.service";
import { Author } from "./authors/author.entity";
import { AuthorsController } from "./authors/authors.controller";
import { AuthorsService } from "./authors/authors.service";

@Module({
  imports: [TypeOrmModule.forFeature([Article, Author])],
  controllers: [ArticlesController, AuthorsController],
  providers: [ArticlesService, AuthorsService]
})
export class BlogModule {}
