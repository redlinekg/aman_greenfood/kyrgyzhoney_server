import { Controller } from "@nestjs/common";
import { Crud, CrudRequest, Override, ParsedRequest } from "@nestjsx/crud";
import { Article } from "./article.entity";
import { ArticlesService } from "./articles.service";

@Crud({
  model: {
    type: Article
  },
  query: {
    join: {
      category: {
        eager: true
      },
      author: {
        eager: true
      },
      "author.avatar": {
        eager: true
      },
      image: {
        eager: true
      },
      meta_image: {
        eager: true
      }
    },
    sort: [
      {
        field: "pin",
        order: "DESC"
      },
      {
        field: "created_at",
        order: "DESC"
      }
    ]
  },
  routes: {
    updateOneBase: {
      allowParamsOverride: true
    }
  },
  params: {
    id: {
      field: "id",
      type: "string",
      primary: true
    }
  }
})
@Controller("api/articles")
export class ArticlesController {
  constructor(readonly service: ArticlesService) {}

  @Override("getOneBase")
  public getByIdOrSlug(@ParsedRequest() req: CrudRequest): Promise<Article> {
    if (!parseInt((req.parsed.search.$and[1] as any).id.$eq)) {
      req.parsed.search.$and.push({
        slug: (req.parsed.search.$and[1] as any).id.$eq
      });
      delete req.parsed.search.$and[1];
    }
    return this.service.getOne(req);
  }
}
