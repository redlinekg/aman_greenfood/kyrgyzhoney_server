import { Injectable } from "@nestjs/common";
import { CrudRequest, CrudRequestOptions } from "@nestjsx/crud";
import { ParsedRequestParams } from "@nestjsx/crud-request";
import { InjectRepository } from "@vlzh/nest-typeorm-i18n";
import { I18nRepository } from "typeorm-i18n";
import { CustomTypeOrmService } from "../../CustomTypeOrmService";
import { Article } from "./article.entity";
import { LessThan, MoreThan } from "typeorm";

@Injectable()
export class ArticlesService extends CustomTypeOrmService<Article> {
  constructor(
    @InjectRepository(Article) readonly repo: I18nRepository<Article>
  ) {
    super(repo);
  }
  createBuilder(
    parsed: ParsedRequestParams,
    options: CrudRequestOptions,
    many = true
  ) {
    this.repo.locale((options as any).language);
    return super.createBuilder(parsed, options, many);
  }

  public async getOne(req: CrudRequest): Promise<Article> {
    const article = await super.getOne(req);
    const [prev_article, next_article] = await Promise.all([
      this.repo.findOne({
        where: {
          created_at: LessThan(article.created_at),
        },
        order: {
          created_at: "DESC",
        },
      }),
      this.repo.findOne({
        where: {
          created_at: MoreThan(new Date(article.created_at.getTime() + 1000)),
        },
        order: {
          created_at: "ASC",
        },
      }),
    ]);
    article.prev_entity = prev_article
      ? {
          slug: prev_article.slug,
          title: prev_article.title,
        }
      : null;
    article.next_entity = next_article
      ? {
          slug: next_article.slug,
          title: next_article.title,
        }
      : null;
    return article;
  }
}
