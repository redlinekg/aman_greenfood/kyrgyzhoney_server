import { Column, Entity, ManyToOne } from "typeorm";

import { Author } from "../authors/author.entity";
import { BaseSeoEntity } from "../../base.entity";
import { I18nColumn } from "typeorm-i18n";
import { UploadsItem } from "../../uploads/uploads.entity";
import { DEFAULT_LANG, LANGUAGES_LIST } from "../../constances";

interface SiblinkEntity {
  slug: string;
  title: string;
}


@Entity("articles")
export class Article extends BaseSeoEntity {
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  title: string;
  @Column({
    unique: true
  })
  slug: string;
  @ManyToOne(() => Author)
  author: Author;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column({
    type: "text"
  })
  description: string;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column({
    type: "text"
  })
  body: string;
  @ManyToOne(() => UploadsItem)
  image: UploadsItem;

  // Закрепить в блоге
  @Column({
    default: false
  })
  pin: boolean;

  public next_entity?: SiblinkEntity;
  public prev_entity?: SiblinkEntity;
}
