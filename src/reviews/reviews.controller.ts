import { Controller } from "@nestjs/common";
import { Crud, CrudRequest, Override, ParsedRequest } from "@nestjsx/crud";
import { Review } from "./review.entity";
import { ReviewsService } from "./reviews.service";

@Crud({
  model: {
    type: Review
  },
  query: {
    join: {
      meta_image: {
        eager: true
      },
      avatar: {
        eager: true
      },
      logo: {
        eager: true
      }
    }
  },
  routes: {
    updateOneBase: {
      allowParamsOverride: true
    }
  },
  params: {
    id: {
      field: "id",
      type: "string",
      primary: true
    }
  }
})
@Controller("api/reviews")
export class ReviewsController {
  constructor(private readonly service: ReviewsService) {}
}
