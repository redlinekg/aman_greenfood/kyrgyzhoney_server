import { Column, Entity, ManyToOne } from "typeorm";
import { I18nColumn } from "typeorm-i18n";
import { BaseSeoEntity } from "../base.entity";
import { DEFAULT_LANG, LANGUAGES_LIST } from "../constances";
import { UploadsItem } from "../uploads/uploads.entity";

@Entity()
export class Review extends BaseSeoEntity {
  @ManyToOne(() => UploadsItem, {
    nullable: true
  })
  avatar: UploadsItem;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  name: string;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  country: string;
  @Column()
  social_type: string;
  @Column()
  social_link: string;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  title: string;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  body: string;
}
