import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@vlzh/nest-typeorm-i18n";
import { I18nRepository } from "typeorm-i18n";
import { CustomTypeOrmService } from "../CustomTypeOrmService";
import { Review } from "./review.entity";

@Injectable()
export class ReviewsService extends CustomTypeOrmService<Review> {
  constructor(@InjectRepository(Review) readonly repo: I18nRepository<Review>) {
    super(repo);
  }
}
