import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@vlzh/nest-typeorm-i18n";
import { Review } from "./review.entity";
import { ReviewsController } from "./reviews.controller";
import { ReviewsService } from "./reviews.service";

@Module({
  imports: [TypeOrmModule.forFeature([Review])],
  controllers: [ReviewsController],
  providers: [ReviewsService]
})
export class ReviewsModule {}
