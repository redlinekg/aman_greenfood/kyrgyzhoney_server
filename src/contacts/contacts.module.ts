import { Module } from "@nestjs/common";
import { Contact } from "./contact.entity";
import { ContactsController } from "./contacts.controller";
import { ContactsService } from "./contacts.service";
import { TypeOrmModule } from "@vlzh/nest-typeorm-i18n";

@Module({
  imports: [TypeOrmModule.forFeature([Contact])],
  controllers: [ContactsController],
  providers: [ContactsService]
})
export class ContactsModule {}
