import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@vlzh/nest-typeorm-i18n";
import { CustomTypeOrmService } from "../CustomTypeOrmService";
import { I18nRepository } from "typeorm-i18n";
import { Contact } from "./contact.entity";

@Injectable()
export class ContactsService extends CustomTypeOrmService<Contact> {
  constructor(
    @InjectRepository(Contact) readonly repo: I18nRepository<Contact>
  ) {
    super(repo);
  }
}
