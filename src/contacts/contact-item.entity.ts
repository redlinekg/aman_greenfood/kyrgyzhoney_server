import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { I18nColumn } from "typeorm-i18n";
import { DEFAULT_LANG, LANGUAGES_LIST } from "../constances";
import { Contact } from "./contact.entity";

@Entity()
export class ContactItem {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  type: string;
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  text: string;
  @ManyToOne(() => Contact, {
    onDelete: "CASCADE"
  })
  contact: Contact;
}
