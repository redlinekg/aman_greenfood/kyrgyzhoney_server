import { Controller } from "@nestjs/common";
import { Crud, CrudRequest, Override, ParsedRequest } from "@nestjsx/crud";
import { Contact } from "./contact.entity";
import { ContactsService } from "./contacts.service";

@Crud({
  model: {
    type: Contact
  },
  query: {
    join: {
      meta_image: {
        eager: true
      },
      items: {
        eager: true
      }
    }
  },
  routes: {
    updateOneBase: {
      allowParamsOverride: true
    }
  },
  params: {
    id: {
      field: "id",
      type: "string",
      primary: true
    }
  }
})
@Controller("api/contacts")
export class ContactsController {
  constructor(private readonly service: ContactsService) {}

  @Override("getOneBase")
  public getByIdOrSlug(@ParsedRequest() req: CrudRequest): Promise<Contact> {
    const id = parseInt(req.parsed.paramsFilter[0].value);
    if (isNaN(id)) {
      req.parsed.paramsFilter[0].field = "slug";
    }
    return this.service.getOne(req);
  }
}
