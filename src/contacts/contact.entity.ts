import { Column, Entity, ManyToOne, OneToMany } from "typeorm";

import { BaseEntity } from "../base.entity";
import { I18nColumn } from "typeorm-i18n";
import { DEFAULT_LANG, LANGUAGES_LIST } from "../constances";
import { ContactItem } from "./contact-item.entity";

@Entity()
export class Contact extends BaseEntity {
  // title
  @I18nColumn({
    default_language: DEFAULT_LANG,
    languages: LANGUAGES_LIST
  })
  @Column()
  title: string;

  //  global
  @Column({
    default: false
  })
  public global: boolean;

  @OneToMany(
    () => ContactItem,
    item => item.contact,
    {
      cascade: true
    }
  )
  items: ContactItem[];
}
