import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { CreateUserDto } from './users.dto';
import { InjectRepository } from '@vlzh/nest-typeorm-i18n';
import { User } from './users.entity';
import { Repository } from 'typeorm';
import { hash } from 'bcrypt';

@Injectable()
export class UsersService {
    public constructor(
        @InjectRepository(User)
        private readonly usersRepository: Repository<User>,
    ) {}
    public async findOne(id: number): Promise<User> {
        const user = await this.usersRepository.findOne(id);
        if (!user)
            throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
        return user;
    }
    public async findOneByEmail(email: string): Promise<User> {
        const user = await this.usersRepository.findOne({
            where: {
                email,
            },
        });
        if (!user)
            throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
        return user;
    }
    public findAll(): Promise<User[]> {
        return this.usersRepository.find();
    }
    public async createUser(data: CreateUserDto): Promise<User> {
        return this.usersRepository.save({
            ...data,
            password: await hash(data.password, 10),
        });
    }
    public async deleteUser(id: number): Promise<User> {
        const user = await this.usersRepository.findOne(id);
        if (!user)
            throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
        await this.usersRepository.delete(user);
        return user;
    }
    public async changeUser(id: number, data: CreateUserDto): Promise<User> {
        let user = await this.usersRepository.findOne(id);
        if (!user)
            throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
        Object.assign(user, {
            ...data,
            password: await hash(data.password || user.password, 10),
        });
        user = await this.usersRepository.save(user);
        return user;
    }
}
