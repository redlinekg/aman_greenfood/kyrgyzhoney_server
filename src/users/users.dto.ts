import { IsEmail, MinLength } from 'class-validator';
export class CreateUserDto {
    @IsEmail()
    public readonly email: string;
    @MinLength(6, {
        message: 'Minimal length of password is 6 chars',
    })
    public readonly password: string;
    public readonly password_confirm: string;
}
