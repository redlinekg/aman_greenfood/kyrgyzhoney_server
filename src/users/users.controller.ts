import { Controller, Get, Param } from '@nestjs/common';
import { User } from './users.entity';
import { UsersService } from './users.service';

@Controller('api/users')
export class UsersController {
    public constructor(private readonly usersService: UsersService) {}
    // @Get()
    // public findAll(): Promise<User[]> {
    //     return this.usersService.findAll();
    // }
    @Get(':id')
    public async findOne(@Param('id') id: string): Promise<User> {
        const user = await this.usersService.findOne(+id);
        console.log(user);
        return user;
    }
    // @UsePipes(new ValidationPipe())
    // @Post()
    // public create(@Body() body: CreateUserDto): Promise<User> {
    //     return this.usersService.createUser(body);
    // }
    // @UsePipes(new ValidationPipe())
    // @Patch(':id')
    // public change(
    //     @Param('id') id: string,
    //     @Body() body: CreateUserDto,
    // ): Promise<User> {
    //     return this.usersService.changeUser(+id, body);
    // }
    // @Delete(':id')
    // public remove(@Param('id') id: string): Promise<User> {
    //     return this.usersService.deleteUser(+id);
    // }
}
