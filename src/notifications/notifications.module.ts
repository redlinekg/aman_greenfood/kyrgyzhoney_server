import { Module } from "@nestjs/common";
import { NotificationsController } from "./notifications.controller";
import { CatalogModule } from "../catalog/catalog.module";

@Module({
  imports: [CatalogModule],
  controllers: [NotificationsController]
})
export class NotificationsModule {}
