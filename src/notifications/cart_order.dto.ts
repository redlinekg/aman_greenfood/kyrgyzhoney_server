export class OrderCartLine {
  product_id: number;
  quantity: number;
}

export class CartOrderDto {
  form_country: string;
  form_name: string;
  form_phone: string;
  lines: OrderCartLine[];
}
