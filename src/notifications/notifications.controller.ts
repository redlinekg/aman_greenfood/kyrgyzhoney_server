import { MailerService } from "@nest-modules/mailer";
import {
  Body,
  Controller,
  Post,
  UsePipes,
  ValidationPipe
} from "@nestjs/common";
import { TelegramService } from "nest-telegram-bot";
import { LogManager } from "nestjs-pino-logger/dist";
import { Logger } from "pino";
import { ConfigService } from "../utils/config/config.service";
import { CallmeOrderDto } from "./callme.dto";
import { CartOrderDto, OrderCartLine } from "./cart_order.dto";
import { table } from "table";
import { ProductsService } from "../catalog/products/products.service";

@Controller("api/notifications")
export class NotificationsController {
  private log: Logger;
  public constructor(
    private readonly telegram_service: TelegramService,
    readonly logManager: LogManager,
    private readonly mailerService: MailerService,
    private readonly configService: ConfigService,
    private readonly productsService: ProductsService
  ) {
    this.log = logManager.getLogger(NotificationsController);
  }

  @Post("callme")
  @UsePipes(new ValidationPipe({ transform: true }))
  public callme(@Body() body: CallmeOrderDto) {
    this.telegram_service.sendMessage({
      message: `
*New callback order*:
Country: ${body.form_country}
Name: ${body.form_name}
Phone number: ${body.form_phone}
      `,
      parse_mode: "Markdown"
    });
    return body;
  }

  @Post("cart")
  @UsePipes(new ValidationPipe({ transform: true }))
  public async cart_order(@Body() body: CartOrderDto): Promise<string> {
    this.telegram_service.sendMessage({
      message: `
*New cart order*:
Country: ${body.form_country}
Name: ${body.form_name}
Phone number: ${body.form_phone}
---
Products:
\`\`\`
${await this.linesToTable(body.lines)}
\`\`\`
      `,
      parse_mode: "Markdown"
    });
    return "OK";
  }

  async linesToTable(lines: OrderCartLine[]): Promise<string> {
    const all_products = await this.productsService.repo.find();
    const data = [
      ["Product id", "Quantity"],
      ...lines.map(line => {
        const product = all_products.find(
          product => product.id === line.product_id
        );
        return [
          product ? product.title : `Unknow product with id ${line.product_id}`,
          line.quantity
        ];
      })
    ];
    return table(data);
  }
}
